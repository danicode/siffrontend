// Modulos
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
// Modulos especiales
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
// Idioma
import localEs from '@angular/common/locales/es-BO';
import { registerLocaleData } from '@angular/common';
// Interceptador
import { IntercepterRequestService } from './services/util/intercepter-request.service';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { UsuarioDashboardComponent } from './components/usuarioSection/usuario-dashboard/usuario-dashboard.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { PersonaDashboardComponent } from './components/personaSection/persona-dashboard/persona-dashboard.component';
import { PersonaRegistroComponent } from './components/personaSection/persona-registro/persona-registro.component';
import { UsuarioPersonaComponent } from './components/usuarioSection/usuario-persona/usuario-persona.component';
import { UsuarioRegistroComponent } from './components/usuarioSection/usuario-registro/usuario-registro.component';
import { UsuarioRolComponent } from './components/usuarioSection/usuario-rol/usuario-rol.component';
import { UsuarioUnidadComponent } from './components/usuarioSection/usuario-unidad/usuario-unidad.component';
import { AutoridadDashboardComponent } from './components/autoridadSection/autoridad-dashboard/autoridad-dashboard.component';
import { AutoridadRegistroEstudianteComponent } from './components/autoridadSection/autoridad-registro-estudiante/autoridad-registro-estudiante.component';
import { EstudianteDashboardComponent } from './components/autoridadSection/estudiante-dashboard/estudiante-dashboard.component';
import { EstudianteRegistroComponent } from './components/autoridadSection/estudiante-registro/estudiante-registro.component';
import { DocenteDashboardComponent } from './components/autoridadSection/docente-dashboard/docente-dashboard.component';
import { DocenteRegistroComponent } from './components/autoridadSection/docente-registro/docente-registro.component';
import { EstudiantePersonaComponent } from './components/autoridadSection/estudiante-persona/estudiante-persona.component';
import { DocentePersonaComponent } from './components/autoridadSection/docente-persona/docente-persona.component';
import { AutoridadRegistroDocenteComponent } from './components/autoridadSection/autoridad-registro-docente/autoridad-registro-docente.component';
import { Autoridad2SectionComponent } from './components/autoridad2-section/autoridad2-section.component';

registerLocaleData(localEs);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    UsuarioDashboardComponent,
    WelcomeComponent,
    PersonaDashboardComponent,
    PersonaRegistroComponent,
    UsuarioPersonaComponent,
    UsuarioRegistroComponent,
    UsuarioRolComponent,
    UsuarioUnidadComponent,
    AutoridadDashboardComponent,
    AutoridadRegistroEstudianteComponent,
    EstudianteDashboardComponent,
    EstudianteRegistroComponent,
    DocenteDashboardComponent,
    DocenteRegistroComponent,
    EstudiantePersonaComponent,
    DocentePersonaComponent,
    AutoridadRegistroDocenteComponent,
    Autoridad2SectionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot()
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es-BO',
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: IntercepterRequestService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
