import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 // Componentes
import { LoginComponent } from './components/login/login.component';
import { UsuarioDashboardComponent } from './components/usuarioSection/usuario-dashboard/usuario-dashboard.component';
import { RouteGuardService } from './services/util/route-guard.service';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { PersonaDashboardComponent } from './components/personaSection/persona-dashboard/persona-dashboard.component';
import { PersonaRegistroComponent } from './components/personaSection/persona-registro/persona-registro.component';
import { UsuarioPersonaComponent } from './components/usuarioSection/usuario-persona/usuario-persona.component';
import { UsuarioRegistroComponent } from './components/usuarioSection/usuario-registro/usuario-registro.component';
import { UsuarioRolComponent } from './components/usuarioSection/usuario-rol/usuario-rol.component';
import { UsuarioUnidadComponent } from './components/usuarioSection/usuario-unidad/usuario-unidad.component';
import { AutoridadDashboardComponent } from './components/autoridadSection/autoridad-dashboard/autoridad-dashboard.component';
import { AutoridadRegistroEstudianteComponent } from './components/autoridadSection/autoridad-registro-estudiante/autoridad-registro-estudiante.component';
import { DocenteDashboardComponent } from './components/autoridadSection/docente-dashboard/docente-dashboard.component';
import { DocenteRegistroComponent } from './components/autoridadSection/docente-registro/docente-registro.component';
import { EstudianteDashboardComponent } from './components/autoridadSection/estudiante-dashboard/estudiante-dashboard.component';
import { EstudianteRegistroComponent } from './components/autoridadSection/estudiante-registro/estudiante-registro.component';
import { EstudiantePersonaComponent } from './components/autoridadSection/estudiante-persona/estudiante-persona.component';
import { DocentePersonaComponent } from './components/autoridadSection/docente-persona/docente-persona.component';
import { AutoridadRegistroDocenteComponent } from './components/autoridadSection/autoridad-registro-docente/autoridad-registro-docente.component';
import { Autoridad2SectionComponent } from './components/autoridad2-section/autoridad2-section.component';

const routes: Routes = [
  { path: 'ingreso', component: LoginComponent},
  { path: 'welcome', component: WelcomeComponent, canActivate: [RouteGuardService]},
  // Persona Section
  { path: 'persona', component: PersonaDashboardComponent, canActivate: [RouteGuardService]},
  { path: 'persona/:idPersona/:mode', component: PersonaRegistroComponent, canActivate: [RouteGuardService]},
  // Usuario Section
  { path: 'usuario', component: UsuarioDashboardComponent, canActivate: [RouteGuardService]},
  { path: 'usuario/persona', component: UsuarioPersonaComponent, canActivate: [RouteGuardService]},
  { path: 'usuario/roles/:idUsuario', component: UsuarioRolComponent, canActivate: [RouteGuardService]},
  { path: 'usuario/unidad/:idUsuario', component: UsuarioUnidadComponent, canActivate: [RouteGuardService]},
  { path: 'usuario/:idUsuario/:idPersona', component: UsuarioRegistroComponent, canActivate: [RouteGuardService]},
  // Autoridad
  { path: 'autoridad', component: AutoridadDashboardComponent, canActivate: [RouteGuardService]},
  { path: 'autoridad/:idUnidadAutoridad', component: AutoridadDashboardComponent, canActivate: [RouteGuardService]},
  { path: 'autoridad/estudiante/:idEstudianteCargo/:idUnidadAutoridad', component: AutoridadRegistroEstudianteComponent, canActivate: [RouteGuardService]},
  { path: 'autoridad/docente/:idDocenteCargo/:idUnidadAutoridad', component: AutoridadRegistroDocenteComponent, canActivate: [RouteGuardService]},
  // Estudiante
  { path: 'estudiante', component: EstudianteDashboardComponent, canActivate: [RouteGuardService]},
  { path: 'estudiante/persona', component: EstudiantePersonaComponent, canActivate: [RouteGuardService]},
  { path: 'estudiante/:idEstudiante/:idPersona', component: EstudianteRegistroComponent, canActivate: [RouteGuardService]},
  // Docente
  { path: 'docente', component: DocenteDashboardComponent, canActivate: [RouteGuardService]},
  { path: 'docente/persona', component: DocentePersonaComponent, canActivate: [RouteGuardService]},
  { path: 'docente/:idDocente/:idPersona', component: DocenteRegistroComponent, canActivate: [RouteGuardService]},
  // Autoridad 2
  { path: 'consultar/docente', component: Autoridad2SectionComponent, canActivate: [RouteGuardService]},


  { path: '', component: WelcomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
