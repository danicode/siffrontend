import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {

  constructor(
    private http: HttpClient
  ) { }

  listarEstudiantes(estudiante: any) {
    return this.http.post(`${API_URL}/api/autoridad/estudiantes/listarEstudiantes`, estudiante);
  }
  obtenerEstudiante(idEstudiante: number) {
    return this.http.get(`${API_URL}/api/autoridad/estudiantes/obtenerEstudiante/${idEstudiante}`);
  }
  registrarEstudiante(estudiante: any) {
    return this.http.post(`${API_URL}/api/autoridad/estudiantes/registrarEstudiante`, estudiante);
  }
  eliminarEstudiante(estudiante: any) {
    return this.http.post(`${API_URL}/api/autoridad/estudiantes/eliminarEstudiante`, estudiante);
  }
  obtenerUnidadAcademica(idUnidad: number) {
    return this.http.get(`${API_URL}/api/autoridad/unidades/obtenerUnidades/${idUnidad}`);
  }
  listarUnidadesAcademicas(unidad: any) {
    return this.http.post(`${API_URL}/api/autoridad/unidades/listarUnidadesAcademicas`, unidad);
  }
}
