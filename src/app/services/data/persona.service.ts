import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  constructor(
    private http: HttpClient
  ) {  }

  listarPersonas(persona: any) {
    return this.http.post(`${API_URL}/api/personas/listarPersonas/`, persona);
  }
  obtenerPersona(idPersona: number) {
    // /${idPersona}
    return this.http.get(`${API_URL}/api/personas/obtenerPersona/${idPersona}`);
  }
  registrarPersona(persona: any) {
    return this.http.post(`${API_URL}/api/personas/registrarPersona`, persona);
  }
  eliminarPersona(persona: any) {
    return this.http.post(`${API_URL}/api/personas/eliminarPersona`, persona);
  }
}
