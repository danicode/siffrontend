import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocenteService {

  constructor(
    private http: HttpClient
  ) { }

  listarDocentes(docente: any) {
    return this.http.post(`${API_URL}/api/autoridad/docentes/listarDocentes`, docente);
  }
  obtenerDocente(idDocente: number) {
    return this.http.get(`${API_URL}/api/autoridad/docentes/obtenerDocente/${idDocente}`);
  }
  registrarDocente(docente: any) {
    return this.http.post(`${API_URL}/api/autoridad/docentes/registrarDocente`, docente);
  }
  eliminarDocente(docente: any) {
    return this.http.post(`${API_URL}/api/autoridad/docentes/eliminarDocente`, docente);
  }
  obtenerUnidadAcademica(idUnidad: number) {
    return this.http.get(`${API_URL}/api/autoridad/unidades/obtenerUnidades/${idUnidad}`);
  }
}
