import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Autoridad2Service {
 /* @Autowired
  EstudianteService estudianteService;*/
  constructor(
    private http: HttpClient
  ) {  } 

  listarDocentes2(docente: any){
    return this.http.post( `${API_URL}/api/autoridad2/docentes/listarDocentes2WebService` , docente);
  }
}
