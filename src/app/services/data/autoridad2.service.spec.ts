import { TestBed } from '@angular/core/testing';

import { Autoridad2Service } from './autoridad2.service';

describe('Autoridad2Service', () => {
  let service: Autoridad2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Autoridad2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
