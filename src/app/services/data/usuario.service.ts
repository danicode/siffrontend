import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: HttpClient
  ) { }

  listarUsuarios(usuario: any) {
    return this.http.post(`${API_URL}/api/usuarios/listarUsuarios/`, usuario);
  }
  obtenerUsuario(idUsuario: number) {
    // /${idUsuario}
    return this.http.get(`${API_URL}/api/usuarios/obtenerUsuario/${idUsuario}`);
  }
  registrarUsuario(usuario: any) {
    return this.http.post(`${API_URL}/api/usuarios/registrarUsuario`, usuario);
  }
  eliminarUsuario(usuario: any) {
    return this.http.post(`${API_URL}/api/usuarios/eliminarUsuario`, usuario);
  }
  listarUsuarioRol(usuarioRol: any) {
    return this.http.post(`${API_URL}/api/usuarios/listarUsuarioRol`, usuarioRol);
  }
  registrarUsuarioRol(usuarioRol: any) {
    return this.http.post(`${API_URL}/api/usuarios/registrarUsuarioRol`, usuarioRol);
  }
  eliminarUsuarioRol(usuarioRol: any) {
    return this.http.post(`${API_URL}/api/usuarios/eliminarUsuarioRol`, usuarioRol);
  }
  listarUsuarioUnidad(usuarioUnidad: any) {
    return this.http.post(`${API_URL}/api/usuarios/listarUsuarioUnidad`, usuarioUnidad);
  }
  registrarUsuarioUnidad(usuarioUnidad: any) {
    return this.http.post(`${API_URL}/api/usuarios/registrarUsuarioUnidad`, usuarioUnidad);
  }
  eliminarUsuarioUnidad(usuarioUnidad: any) {
    return this.http.post(`${API_URL}/api/usuarios/eliminarUsuarioUnidad`, usuarioUnidad);
  }
}
