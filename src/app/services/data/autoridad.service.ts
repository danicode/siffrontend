import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutoridadService {

  constructor(
    private http: HttpClient
  ) { }
  listarEstudianteCargo(autoridad: any) {
    return this.http.post(`${API_URL}/api/autoridad/estudianteCargo/listarEstudianteCargo`, autoridad);
  }

  listarTotalAutoridades(autoridad: any) {
    return this.http.post(`${API_URL}/api/autoridad/estudianteCargo/listarTotalAutoridades`, autoridad);
  }

  obtenerEstudianteCargo(idEstudianteCargo: number) {
    // /${idUsuario}
    return this.http.get(`${API_URL}/api/autoridad/estudianteCargo/obtenerEstudianteCargo/${idEstudianteCargo}`);
  }
  registrarEstudianteCargo(estudianteCargo: any) {
    return this.http.post(`${API_URL}/api/autoridad/estudianteCargo/registrarEstudianteCargo`, estudianteCargo);
  }

  eliminarEstudianteCargo(estudianteCargo: any) {
    return this.http.post(`${API_URL}/api/autoridad/estudianteCargo/eliminarEstudianteCargo`, estudianteCargo);
  }

  listarDocenteCargo(docenteCargo: any) {
    return this.http.post(`${API_URL}/api/autoridad/docenteCargo/listarDocenteCargo`, docenteCargo);
  }

  obtenerDocenteCargo(idDocenteCargo: number) {
    // /${idUsuario}
    return this.http.get(`${API_URL}/api/autoridad/docenteCargo/obtenerDocenteCargo/${idDocenteCargo}`);
  }
  registrarDocenteCargo(docenteCargo: any) {
    return this.http.post(`${API_URL}/api/autoridad/docenteCargo/registrarDocenteCargo`, docenteCargo);
  }

  eliminarDocenteCargo(docenteCargo: any) {
    return this.http.post(`${API_URL}/api/autoridad/docenteCargo/eliminarDocenteCargo`, docenteCargo);
  }
  listarEstudiantesMatriculacion(ci: string, idUsuario: number) {
    return this.http.get(`${API_URL}/api/autoridad/estudiantes/listarEstudiantesMatriculacion/${ci}/${idUsuario}`);
  }

  listarUnidades(autoridad: any) {
    return this.http.post(`${API_URL}/api/autoridad/unidades/listarUsuarioUnidad`, autoridad);
  }

  listarCargoEstudiantil(cargoEstudiantil: any) {
    return this.http.post(`${API_URL}/api/autoridad/listarCargoEstudiantil`, cargoEstudiantil);
  }

  listarTipoCargoNivel() {
    return this.http.get(`${API_URL}/api/autoridad/listarTipoCargoNivel`);
  }

  listarTipoCargoNro() {
    return this.http.get(`${API_URL}/api/autoridad/listarTipoCargoNro`);
  }

  listarDocentesWebService(ci: string, idUsuario: number) {
    return this.http.get(`${API_URL}/api/autoridad/docentes/listarDocentesWebService/${ci}/${idUsuario}`);
  }


  // obtenerPersona(idPersona: number) {
  //   // /${idPersona}
  //   return this.http.get(`${API_URL}/api/personas/obtenerPersona/${idPersona}`);
  // }
  // registrarPersona(persona: any) {
  //   return this.http.post(`${API_URL}/api/personas/registrarPersona`, persona);
  // }
  // eliminarPersona(persona: any) {
  //   return this.http.post(`${API_URL}/api/personas/eliminarPersona`, persona);
  // }

  reporte(idUnidadAutoridad: number) {
    const httpOptions = {
      responseType: 'arraybuffer' as 'json'
      // 'responseType'  : 'blob' as 'json'        //This also worked
    };
    return this.http.get(`${API_URL}/api/print/${idUnidadAutoridad}`, httpOptions);
  }

  reporteExcel(idUnidadAutoridad: number) {
    const httpOptions = {
      //responseType: 'arraybuffer' as 'json'
       responseType  : 'blob' as 'json'
    };
    return this.http.get(`${API_URL}/api/print/xlsx/${idUnidadAutoridad}`, httpOptions);
  }
}
