import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthenticationService } from './../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class IntercepterRequestService implements HttpInterceptor {

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
  const token = this.authenticationService.getAuthenticatedToken();
  if ( token ) {
    request = request.clone({
      setHeaders: {
        Authorization: token
      }
    });
  }
  return next.handle(request);
  }
}
