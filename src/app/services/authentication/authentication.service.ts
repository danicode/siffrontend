import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { API_URL, TOKEN, AUTHENTICATED_USER, ROLES } from './../../../environments/environment';
import * as jwt_decode from 'jwt-decode';


@Injectable({
  providedIn: 'root'
})


export class AuthenticationService {

  constructor(
    private http: HttpClient
  ) { }
  authenticate(username: string, password: string) {
    const body = {username, password};
    return this.http.post(`${API_URL}/login`, body, { observe: 'response' } )
      .pipe(
        map(
          data => {
            sessionStorage.setItem(TOKEN, data.headers.get('Authorization'));
          }
        )
      );
  }
  getAuthenticatedUser() {
    return sessionStorage.getItem(TOKEN);
  }
  getAuthenticatedToken() {
    if (this.getAuthenticatedUser()) {
      return sessionStorage.getItem(TOKEN);
    }
  }
  isAuthenticated() {
    const user = sessionStorage.getItem(TOKEN);
    return !(user === null);
  }

  getUser() {
    if (!this.isAuthenticated()) {
      return false;
    }
    const token: string = this.getAuthenticatedToken();
    const usuario = jwt_decode(token.substr(7, token.length)).id_Usuario;
    if (token && usuario) {
      return usuario;
    }
    return false;
  }
  isROLE_ADMIN() {
    if (!this.isAuthenticated()) {
      return false;
    }
    const token: string = this.getAuthenticatedToken();
    const authorities = jwt_decode(token.substr(7, token.length)).authorities;
    if (token && authorities) {
      return authorities.map( item => item.authority).includes(ROLES.ROLE_ADMIN);
    }
    return false;
  }
  isROLE_ESTUDIANTE() {
    if (!this.isAuthenticated()) {
      return false;
    }
    const token: string = this.getAuthenticatedToken();
    const authorities = jwt_decode(token.substr(7, token.length)).authorities;
    if (token && authorities) {
      return authorities.map( item => item.authority).includes(ROLES.ROLE_ESTUDIANTE);
    }
    return false;
  }

  isROLE_AUTORIDAD_2() {
    if (!this.isAuthenticated()) {
      return false;
    }
    const token: string = this.getAuthenticatedToken();
    const authorities = jwt_decode(token.substr(7, token.length)).authorities;
    if (token && authorities) {
      return authorities.map( item => item.authority).includes(ROLES.ROLE_AUTORIDAD_2);
    }
    return false;
  }


  logout() {
    sessionStorage.removeItem(TOKEN);
  }
}
