import { Component, OnInit, TemplateRef } from '@angular/core';
import { PersonaService } from '../../../services/data/persona.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'app-persona-dashboard',
  templateUrl: './persona-dashboard.component.html',
  styleUrls: ['./persona-dashboard.component.css']
})
export class PersonaDashboardComponent implements OnInit {
  // Variables de Administracion
  personas: any[];
  persona: any;
  idPersona: number;
  // Variables de Interfaz
  busqueda: string;
  searchText: string;
  p = 1;
  modalRef: BsModalRef;
  error = false;
  errorMensaje: string;
  constructor(
    private personaService: PersonaService,
    private route: Router,
    private modalService: BsModalService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.persona = {
      // idPersona: null
    };
    this.personaService.listarPersonas(this.persona)
      .subscribe(
        (data: any) => this.personas = data
      );
  }
  verPersona(idPersona: number) {
    this.route.navigate(['/persona/', idPersona, 0]);
  }
  registrarPersona(idPersona: number) {
    this.route.navigate(['/persona/', idPersona, 1]);
  }
  eliminarPersona(idPersona: number) {
    this.persona.idPersona = idPersona;
    this.persona.usuMod = this.authenticationService.getUser();
    this.personaService.eliminarPersona(this.persona)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            this.ngOnInit();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
  }
  openModal(template: TemplateRef<any>, idPersona: number) {
    this.idPersona = idPersona;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
  confirm() {
    this.eliminarPersona(this.idPersona);
    this.modalRef.hide();
  }
  decline() {
    this.modalRef.hide();
  }
}
