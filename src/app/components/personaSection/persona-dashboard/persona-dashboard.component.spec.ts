import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaDashboardComponent } from './persona-dashboard.component';

describe('PersonaDashboardComponent', () => {
  let component: PersonaDashboardComponent;
  let fixture: ComponentFixture<PersonaDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonaDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonaDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
