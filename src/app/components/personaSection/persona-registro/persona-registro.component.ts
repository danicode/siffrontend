import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../../services/data/persona.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BsDatepickerConfig, DatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-persona-registro',
  templateUrl: './persona-registro.component.html',
  styleUrls: ['./persona-registro.component.css']
})
export class PersonaRegistroComponent implements OnInit {
  persona: any = {};

  sexoList = [{ idSexo: 1, descripcion: 'Hombre'}, { idSexo: 2, descripcion: 'Mujer'}];
  expedidoList = [
     'LP',
     'CB',
     'OR',
     'CH',
     'PT',
     'TJ',
     'SC',
     'BN',
     'PD'
  ];
  editar = false;
  error = false;
  errorMensaje: string;
  // para fechas
  datePickerConfig: Partial<BsDatepickerConfig>;
  constructor(
    private personaService: PersonaService,
    private route: Router,
    private activeRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private bsLocaleService: BsLocaleService
  ) {
    // para fechas
    this.bsLocaleService.use('es');
  }

  ngOnInit(): void {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: false,
        dateInputFormat: 'DD/MM/YYYY',
        maxDate: new Date(2006, 1, 1)
      });
    this.persona = {
        idPersona: this.activeRoute.snapshot.params.idPersona
    };
    this.editar = this.activeRoute.snapshot.params.mode === '1';
    this.personaService.obtenerPersona(this.persona.idPersona)
      .subscribe(
        (data: any) => {
          this.persona = data;
        });
  }
  volver() {
    this.route.navigate(['persona']);
  }
  registrarPersona(idPersona: number) {
    this.persona.idPersona = idPersona === 0 ? null : idPersona;
    this.persona.usuReg = this.authenticationService.getUser();
    this.persona.usuMod = this.authenticationService.getUser();
    this.personaService.registrarPersona(this.persona)
      .subscribe(
        (data: any ) => {
          if (data.code === 1) {
            this.volver();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
  }

}
