import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Autoridad2SectionComponent } from './autoridad2-section.component';

describe('Autoridad2SectionComponent', () => {
  let component: Autoridad2SectionComponent;
  let fixture: ComponentFixture<Autoridad2SectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Autoridad2SectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Autoridad2SectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
