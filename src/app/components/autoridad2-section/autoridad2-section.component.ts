import { Component, OnInit } from '@angular/core';
import { Autoridad2Service } from 'src/app/services/data/autoridad2.service';


@Component({
  selector: 'app-autoridad2-section',
  templateUrl: './autoridad2-section.component.html',
  styleUrls: ['./autoridad2-section.component.css']
})
export class Autoridad2SectionComponent implements OnInit {

  // Variables de Administracion
  docente: any;
  docentes: any[];
  // Variables de Interfaz
  busqueda: string;
  searchText: string;
  
  p = 1;
  error = false;
  errorMensaje: string;
  constructor(
    private autoridad2Service: Autoridad2Service
  ) { }
  ngOnInit(): void {
  }

  buscarDocente(){
    //(this.searchText != '' || this.searchText != null) ? '___' :
    this.docente = {
      nombreCompleto: this.searchText 
    };

    this.autoridad2Service.listarDocentes2(this.docente).subscribe(
      (data: any) => {
        this.docentes = data;
      }
    );
  }


}
