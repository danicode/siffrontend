import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../../services/data/usuario.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-rol',
  templateUrl: './usuario-rol.component.html',
  styleUrls: ['./usuario-rol.component.css']
})
export class UsuarioRolComponent implements OnInit {

   // Variables de Administracion
   usuarioRoles: any[];
   usuarioRol: any;
   usuario: any;
   idRol: number;
   // Variables de Interfaz
   error = false;
   errorMensaje: string;
   constructor(
     private usuarioService: UsuarioService,
     private route: Router,
     private activeRoute: ActivatedRoute,
     private authenticationService: AuthenticationService
   ) { }

   ngOnInit(): void {
    this.usuarioRol = {
      idUsuario: this.activeRoute.snapshot.params.idUsuario
    };
    this.usuario = {
      idUsuario: this.activeRoute.snapshot.params.idUsuario
    };
    this.usuarioService.listarUsuarioRol(this.usuarioRol)
      .subscribe(
        (data: any) => this.usuarioRoles = data
      );
    this.usuarioService.obtenerUsuario(this.usuario.idUsuario)
      .subscribe(
        (data: any) => this.usuario = data
      );
  }
  volver() {
    this.route.navigate(['usuario']);
  }
  editarRol(idUsuario, idRol, event) {
    console.log(idUsuario);
    console.log(idRol);
    console.log(event.target.checked);
    this.usuarioRol = {
      idUsuario,
      idRol
    };
    if (event.target.checked) {
      this.usuarioService.registrarUsuarioRol(this.usuarioRol)
        .subscribe(
          (data: any) => {
            if (data.code === 1) {
              // this.volver();
            } else {
              this.error = true;
              this.errorMensaje = data.message;
            }
          }
        );
    } else {
      this.usuarioService.eliminarUsuarioRol(this.usuarioRol)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            // this.volver();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
    }

  }

}
