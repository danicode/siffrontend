import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { UsuarioService } from '../../../services/data/usuario.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-usuario-dashboard',
  templateUrl: './usuario-dashboard.component.html',
  styleUrls: ['./usuario-dashboard.component.css']
})
export class UsuarioDashboardComponent implements OnInit {

  // Variables de Administracion
  usuarios: any[];
  usuario: any;
  idUsuario: number;
  // Variables de Interfaz
  busqueda: string;
  searchText: string;
  p = 1;
  modalRef: BsModalRef;
  error = false;
  errorMensaje: string;
  constructor(
    private usuarioService: UsuarioService,
    private route: Router,
    private modalService: BsModalService,
    private authenticationService: AuthenticationService
  ) { }
  ngOnInit(): void {
    this.usuario = {
      // idPersona: null
    };
    this.usuarioService.listarUsuarios(this.usuario)
      .subscribe(
        (data: any) => this.usuarios = data
      );
  }
  registrarUsuario(idUsuario: number) {
    if (idUsuario === 0) {
      this.route.navigate(['usuario', 'persona']);
    } else {
      this.route.navigate(['usuario', idUsuario, 0]);
    }
  }
  eliminarPersona(idUsuario: number) {
    this.usuario.idUsuario = idUsuario;
    this.usuario.usuMod = this.authenticationService.getUser();
    this.usuarioService.eliminarUsuario(this.usuario)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            this.ngOnInit();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
  }
  openModal(template: TemplateRef<any>, idUsuario: number) {
    this.idUsuario = idUsuario;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
  confirm() {
    this.eliminarPersona(this.idUsuario);
    this.modalRef.hide();
  }
  decline() {
    this.modalRef.hide();
  }
  registrarRoles(idUsuario: number) {
    this.route.navigate(['usuario', 'roles', idUsuario]);
  }
  registrarUnidades(idUsuario: number) {
    this.route.navigate(['usuario', 'unidad', idUsuario]);
  }
}
