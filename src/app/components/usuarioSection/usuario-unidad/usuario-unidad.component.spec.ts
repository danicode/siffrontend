import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioUnidadComponent } from './usuario-unidad.component';

describe('UsuarioUnidadComponent', () => {
  let component: UsuarioUnidadComponent;
  let fixture: ComponentFixture<UsuarioUnidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuarioUnidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuarioUnidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
