import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../../services/data/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'app-usuario-unidad',
  templateUrl: './usuario-unidad.component.html',
  styleUrls: ['./usuario-unidad.component.css']
})
export class UsuarioUnidadComponent implements OnInit {
 // Variables de Administracion
 usuarioUnidades: any[];
 usuarioUnidad: any = {};
 usuario: any = {};
 idUnidad: number;
 // Variables de Interfaz
 searchText: string;
 error = false;
 errorMensaje: string;
 constructor(
   private usuarioService: UsuarioService,
   private route: Router,
   private activeRoute: ActivatedRoute,
   private authenticationService: AuthenticationService
 ) { }

 ngOnInit(): void {
  this.usuarioUnidad.idUsuario = this.activeRoute.snapshot.params.idUsuario;
  this.usuarioService.listarUsuarioUnidad(this.usuarioUnidad)
    .subscribe(
      (data: any) => this.usuarioUnidades = data
    );
  this.usuario.idUsuario = this.activeRoute.snapshot.params.idUsuario;
  this.usuarioService.obtenerUsuario(this.usuario.idUsuario)
    .subscribe(
      (data: any) => this.usuario = data
    );
}
volver() {
  this.route.navigate(['usuario']);
}
editarUnidad(idUsuario, idUnidad, event) {
  console.log(idUsuario);
  console.log(idUnidad);
  console.log(event.target.checked);
  this.usuarioUnidad = {
    idUsuario,
    idUnidad
  };
  if (event.target.checked) {
    this.usuarioService.registrarUsuarioUnidad(this.usuarioUnidad)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            // this.volver();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
  } else {
    this.usuarioService.eliminarUsuarioUnidad(this.usuarioUnidad)
    .subscribe(
      (data: any) => {
        if (data.code === 1) {
          // this.volver();
        } else {
          this.error = true;
          this.errorMensaje = data.message;
        }
      }
    );
  }

}
}
