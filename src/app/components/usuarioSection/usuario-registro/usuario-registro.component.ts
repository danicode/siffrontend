import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../../services/data/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { PersonaService } from '../../../services/data/persona.service';

@Component({
  selector: 'app-usuario-registro',
  templateUrl: './usuario-registro.component.html',
  styleUrls: ['./usuario-registro.component.css']
})
export class UsuarioRegistroComponent implements OnInit {

  usuario: any = {};
  editar = false;
  error = false;
  errorMensaje: string;
  constructor(
    private usuarioService: UsuarioService,
    private personaService: PersonaService,
    private route: Router,
    private activeRoute: ActivatedRoute,
    private authenticationService: AuthenticationService
  ) { }
  ngOnInit(): void {
    this.usuario = {
        idUsuario: this.activeRoute.snapshot.params.idUsuario,
        idPersona: this.activeRoute.snapshot.params.idPersona
    };
    if (this.usuario.idUsuario === '0') {
      this.personaService.obtenerPersona(this.usuario.idPersona)
      .subscribe(
        (data: any) => {
          this.usuario = {
            nombreCompleto: data.nombreCompleto,
            idPersona: this.activeRoute.snapshot.params.idPersona
          };
        });
    } else {
      this.usuarioService.obtenerUsuario(this.usuario.idUsuario)
      .subscribe(
        (data: any) => {
          this.usuario = data;
        });
    }
  }
  volver() {
    this.route.navigate(['usuario']);
  }
  registrarUsuario(idUsuario: number) {
    this.usuario.idUsuario = idUsuario === 0 ? null : idUsuario;
    this.usuario.usuReg = this.authenticationService.getUser();
    this.usuario.usuMod = this.authenticationService.getUser();
    this.usuarioService.registrarUsuario(this.usuario)
      .subscribe(
        (data: any ) => {
          if (data.code === 1) {
            this.volver();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        });
  }
}
