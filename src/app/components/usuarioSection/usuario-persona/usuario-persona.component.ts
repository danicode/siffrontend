import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../../services/data/persona.service';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'app-usuario-persona',
  templateUrl: './usuario-persona.component.html',
  styleUrls: ['./usuario-persona.component.css']
})
export class UsuarioPersonaComponent implements OnInit {
  // Variables de Administracion
  personas: any[];
  persona: any;
  idPersona: number;
  // Variables de Interfaz
  searchText: string;
  p = 1;
  modalRef: BsModalRef;
  error = false;
  errorMensaje: string;
  constructor(
    private personaService: PersonaService,
    private route: Router,
    private modalService: BsModalService,
    private authenticationService: AuthenticationService
  ) { }
  ngOnInit(): void {
    this.persona = {
      // idPersona: null
    };
    this.personaService.listarPersonas(this.persona)
      .subscribe(
        (data: any) => this.personas = data
      );
  }
  volver() {
    this.route.navigate(['usuario']);
  }
  seleccionarPersona(idPersona: number) {
    this.route.navigate(['usuario', 0, idPersona]);
  }
}
