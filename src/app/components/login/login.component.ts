import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  errorLogin = false;
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.authenticationService.logout();
  }

  handleLogin() {
    this.authenticationService.authenticate(this.username, this.password)
        .subscribe(
          response => {
            this.router.navigate(['welcome']);
          },
          error => {
            this.errorLogin = true;
          }
        );
  }
}
