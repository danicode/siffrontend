import { Component, OnInit } from '@angular/core';
import { DocenteService } from '../../../services/data/docente.service';
import { AutoridadService } from '../../../services/data/autoridad.service';
import { PersonaService } from '../../../services/data/persona.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { EstudianteService } from '../../../services/data/estudiante.service';

@Component({
  selector: 'app-docente-registro',
  templateUrl: './docente-registro.component.html',
  styleUrls: ['./docente-registro.component.css']
})
export class DocenteRegistroComponent implements OnInit {

  docente: any = {};
  unidades: any[];
  error = false;
  errorMensaje: string;
  constructor(
    private docenteService: DocenteService,
    private autoridadService: AutoridadService,
    private personaService: PersonaService,
    private estudianteService: EstudianteService,
    private route: Router,
    private activeRoute: ActivatedRoute,
    private authenticationService: AuthenticationService
  ) { }
  ngOnInit(): void {
    this.estudianteService.listarUnidadesAcademicas({nivel: 3})
      .subscribe(
        (data: any) => {
          this.unidades = data;
        }

      ),

    this.docente = {
        idDocente: this.activeRoute.snapshot.params.idDocente,
        idPersona: this.activeRoute.snapshot.params.idPersona
    };
    if (this.docente.idDocente === '0') {
      this.personaService.obtenerPersona(this.docente.idPersona)
      .subscribe(
        (data: any) => {
          this.docente = {
            nombreCompleto: data.nombreCompleto,
            idPersona: this.activeRoute.snapshot.params.idPersona,
            idUnidad: null
          };
        });
    } else {
      this.docenteService.obtenerDocente(this.docente.idDocente)
      .subscribe(
        (data: any) => {
          this.docente = data;
        });
    }
  }
  volver() {
    this.route.navigate(['docente']);
  }
  registrarDocente(idDocente: number) {
    this.docente.idDocente = idDocente === 0 ? null : idDocente;
    this.docente.usuReg = this.authenticationService.getUser();
    this.docente.usuMod = this.authenticationService.getUser();
    this.docenteService.registrarDocente(this.docente)
      .subscribe(
        (data: any ) => {
          if (data.code === 1) {
            this.volver();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        });
  }

}
