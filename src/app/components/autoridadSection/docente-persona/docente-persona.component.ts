import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonaService } from '../../../services/data/persona.service';

@Component({
  selector: 'app-docente-persona',
  templateUrl: './docente-persona.component.html',
  styleUrls: ['./docente-persona.component.css']
})
export class DocentePersonaComponent implements OnInit {

 // Variables de Administracion
 personas: any[];
 persona: any;
 idPersona: number;
 // Variables de Interfaz
 searchText: string;
 p = 1;
 error = false;
 errorMensaje: string;
 constructor(
   private personaService: PersonaService,
   private route: Router
 ) { }
 ngOnInit(): void {
   this.persona = {
     // idPersona: null
   };
   this.personaService.listarPersonas(this.persona)
     .subscribe(
       (data: any) => this.personas = data
     );
 }
 volver() {
   this.route.navigate(['docente']);
 }
 seleccionarPersona(idPersona: number) {
   this.route.navigate(['docente', 0, idPersona]);
 }

}
