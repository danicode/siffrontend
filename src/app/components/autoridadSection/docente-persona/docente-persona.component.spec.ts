import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocentePersonaComponent } from './docente-persona.component';

describe('DocentePersonaComponent', () => {
  let component: DocentePersonaComponent;
  let fixture: ComponentFixture<DocentePersonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocentePersonaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocentePersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
