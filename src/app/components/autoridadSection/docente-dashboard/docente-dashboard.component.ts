import { Component, OnInit, TemplateRef } from '@angular/core';
import { DocenteService } from '../../../services/data/docente.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'app-docente-dashboard',
  templateUrl: './docente-dashboard.component.html',
  styleUrls: ['./docente-dashboard.component.css']
})
export class DocenteDashboardComponent implements OnInit {
  // Variables de Administracion
  docentes: any[];
  docente: any;
  idDocente: number;
  // Variables de Interfaz
  busqueda: string;
  searchText: string;
  p = 1;
  modalRef: BsModalRef;
  error = false;
  errorMensaje: string;
  constructor(
    private docenteService: DocenteService,
    private route: Router,
    private modalService: BsModalService,
    private authenticationService: AuthenticationService
  ) { }
  ngOnInit(): void {
    this.docente = {
      // idPersona: null
    };
    
  }
  buscarDocente(){
    this.docenteService.listarDocentes({nroCi:this.searchText})
      .subscribe(
        (data: any) => this.docentes = data
      );
  }
  registrarDocente(idDocente: number) {
    if (idDocente === 0) {
      this.route.navigate(['docente', 'persona']);
    } else {
      this.route.navigate(['docente', idDocente, 0]);
    }
  }
  eliminarDocente(idDocente: number) {
    this.docente.idDocente = idDocente;
    this.docente.usuMod = this.authenticationService.getUser();
    this.docenteService.eliminarDocente(this.docente)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            this.ngOnInit();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
  }
  openModal(template: TemplateRef<any>, idDocente: number) {
    this.idDocente = idDocente;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
  confirm() {
    this.eliminarDocente(this.idDocente);
    this.modalRef.hide();
  }
  decline() {
    this.modalRef.hide();
  }
}
