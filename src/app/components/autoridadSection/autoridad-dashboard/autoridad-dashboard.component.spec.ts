import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoridadDashboardComponent } from './autoridad-dashboard.component';

describe('AutoridadDashboardComponent', () => {
  let component: AutoridadDashboardComponent;
  let fixture: ComponentFixture<AutoridadDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoridadDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoridadDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
