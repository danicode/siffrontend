import { Component, OnInit, TemplateRef } from '@angular/core';
import { AutoridadService } from '../../../services/data/autoridad.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal/public_api';
import { BsModalService } from 'ngx-bootstrap/modal';
import { saveAs } from 'file-saver';


@Component({
  selector: 'app-autoridad-dashboard',
  templateUrl: './autoridad-dashboard.component.html',
  styleUrls: ['./autoridad-dashboard.component.css']
})
export class AutoridadDashboardComponent implements OnInit {
  unidades: any[];
  autoridades: any[];
  autoridad: any = {idUnidadAutoridad: null};
  idEstudianteCargo: number;
  // Variables de Interfaz
  busqueda: string;
  searchText: string;
  p = 1;
  modalRef: BsModalRef;
  error = false;
  errorMensaje: string;
  constructor(
    private autoridadService: AutoridadService,
    private authenticationService: AuthenticationService,
    private modalService: BsModalService,
    private router: Router,
    private activeRouter: ActivatedRoute
  ) { }

  ngOnInit(): void {
    console.log(this.activeRouter.snapshot.params.idUnidadAutoridad);
    this.autoridad.idUsuario = this.authenticationService.getUser();
    this.autoridad.all = 1;
    this.autoridadService.listarUnidades(this.autoridad)
      .subscribe(
        (data: any) => {
          this.unidades = data;
          if (this.unidades.length > 0) {
            if (this.activeRouter.snapshot.params.idUnidadAutoridad) {
              this.autoridad.idUnidadAutoridad = this.activeRouter.snapshot.params.idUnidadAutoridad;
            } else {
              this.autoridad.idUnidadAutoridad = this.unidades[0].idUnidad;
            }
            this.listarAutoridadesCargo();
          }
        }
      );
  }

  listarAutoridadesCargo() {
    this.autoridadService.listarTotalAutoridades(
      {
        all: 1,
        idUnidadAutoridad: this.autoridad.idUnidadAutoridad
      }
    )
      .subscribe(
        (data: any) => {
          this.autoridades = data;
        }
      );
  }

  registrarAutoridadEstudiante(idEstudianteCargo: number) {
    this.router.navigate(['autoridad', 'estudiante', idEstudianteCargo, this.autoridad.idUnidadAutoridad]);
  }

  registrarAutoridadDocente(idDocenteCargo: number) {
    this.router.navigate(['autoridad', 'docente', idDocenteCargo, this.autoridad.idUnidadAutoridad]);
  }

  reportePorUnidad() {
  
    this.autoridadService.reporte(this.autoridad.idUnidadAutoridad)
    .subscribe(
      (data: any) => {
        const file = new Blob([data], { type: 'application/pdf' });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      }
    );
  }

  reportePorUnidadExcel() {
    this.autoridadService.reporteExcel(this.autoridad.idUnidadAutoridad)
    .subscribe(
      (data: any) => {
       /* const file = new Blob([data], { type: 'application/ms-excel' });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL,'_blank','');*/
        const blob = new Blob([data], { type : 'application/vnd.ms.excel' });
        const file = new File([blob], 'cobe2.xls', { type: 'application/vnd.ms.excel' });
        saveAs(file);
      }
    );
  }

  eliminarEstudianteCargo(idEstudianteCargo: number) {
    this.autoridad.idEstudianteCargo = idEstudianteCargo;
    this.autoridad.usuMod = this.authenticationService.getUser();
    this.autoridadService.eliminarEstudianteCargo(this.autoridad)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            this.listarAutoridadesCargo();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
  }
  openModalCargoEstudiante(template: TemplateRef<any>, idEstudianteCargo: number) {
    this.idEstudianteCargo = idEstudianteCargo;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
  confirmCargoEstudiante() {
    this.eliminarEstudianteCargo(this.idEstudianteCargo);
    this.modalRef.hide();
  }
  declineCargoEstudiante() {
    this.modalRef.hide();
  }
  eliminarDocenteCargo(idDocenteCargo: number) {
    this.autoridadService.eliminarDocenteCargo(
      {
        idDocenteCargo,
        usuMod: this.authenticationService.getUser()
      }
    )
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            this.listarAutoridadesCargo();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
  }
  openModalCargoDocente(template: TemplateRef<any>, idDocenteCargo: number) {
    this.idEstudianteCargo = idDocenteCargo;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
  confirmCargoDocente() {
    this.eliminarDocenteCargo(this.idEstudianteCargo);
    this.modalRef.hide();
  }
  declineCargoDocente() {
    this.modalRef.hide();
  }
}
