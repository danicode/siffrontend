import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/public_api';
import { EstudianteService } from '../../../services/data/estudiante.service';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'app-estudiante-dashboard',
  templateUrl: './estudiante-dashboard.component.html',
  styleUrls: ['./estudiante-dashboard.component.css']
})
export class EstudianteDashboardComponent implements OnInit {
  // Variables de Administracion
  estudiantes: any[];
  estudiante: any;
  idEstudiante: number;
  // Variables de Interfaz
  busqueda: string;
  searchText: string;
  p = 1;
  modalRef: BsModalRef;
  error = false;
  errorMensaje: string;
  constructor(
    private estudianteService: EstudianteService,
    private route: Router,
    private modalService: BsModalService,
    private authenticationService: AuthenticationService
  ) { }
  ngOnInit(): void {
    this.estudiante = {
      // idPersona: null
    };
  }
  buscarEstudiantes() {
    this.estudianteService.listarEstudiantes({nroCi: this.searchText})
      .subscribe(
        (data: any) => this.estudiantes = data
      );
  }
  registrarEstudiante(idEstudiante: number) {
    if (idEstudiante === 0) {
      this.route.navigate(['estudiante', 'persona']);
    } else {
      this.route.navigate(['estudiante', idEstudiante, 0]);
    }
  }
  eliminarEstudiante(idEstudiante: number) {
    this.estudiante.idEstudiante = idEstudiante;
    this.estudiante.usuMod = this.authenticationService.getUser();
    this.estudianteService.eliminarEstudiante(this.estudiante)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            this.ngOnInit();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        }
      );
  }
  openModal(template: TemplateRef<any>, idEstudiante: number) {
    this.idEstudiante = idEstudiante;
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }
  confirm() {
    this.eliminarEstudiante(this.idEstudiante);
    this.modalRef.hide();
  }
  decline() {
    this.modalRef.hide();
  }
}
