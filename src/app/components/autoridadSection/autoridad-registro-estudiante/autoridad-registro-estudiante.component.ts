import { Component, OnInit } from '@angular/core';
import { Router, RouterLinkActive, ActivatedRoute } from '@angular/router';
import { AutoridadService } from '../../../services/data/autoridad.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { EstudianteService } from '../../../services/data/estudiante.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/public_api';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-autoridad-registro-estudiante',
  templateUrl: './autoridad-registro-estudiante.component.html',
  styleUrls: ['./autoridad-registro-estudiante.component.css']
})
export class AutoridadRegistroEstudianteComponent implements OnInit {
  //
  estudianteCargo: any = {};
  busquedaCi: string;
  estudiantes: any[];
  estudiante: any = {};
  idUnidad: number;
  unidadAutoridad: any = {};
  cargoEstudiantil: any [];
  tipoCargoNivel: any [];
  tipoCargoNro: any [];
  // para fechas
  datePickerConfig: Partial<BsDatepickerConfig>;
  formularioCompleto = true;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private autoridadService: AutoridadService,
    private estudianteService: EstudianteService,
    private bsLocaleService: BsLocaleService
  ) {
    // para fechas
    this.bsLocaleService.use('es');
   }

  ngOnInit(): void {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: false,
        dateInputFormat: 'DD/MM/YYYY'
      });
    this.idUnidad = this.activatedRoute.snapshot.params.idUnidadAutoridad;
    this.estudianteCargo.idEstudianteCargo = this.activatedRoute.snapshot.params.idEstudianteCargo;
    if (this.estudianteCargo.idEstudianteCargo !== '0') {
      this.formularioCompleto = false;
    }

    this.autoridadService.obtenerEstudianteCargo(this.estudianteCargo.idEstudianteCargo)
      .subscribe(
        (data: any) => {
          this.estudianteCargo = data;
          if ( this.estudianteCargo.idEstudianteCargo !== 0) {
            this.estudiante.nombreCompleto = this.estudianteCargo.nombreCompleto;
            this.estudiante.unidadAcademica = this.estudianteCargo.unidadAcademica;
            this.estudiante.ru = this.estudianteCargo.ru;
          }
        });

    this.estudianteService.obtenerUnidadAcademica(this.idUnidad)
      .subscribe(
        (data: any ) => {
          this.unidadAutoridad = data;
          this.autoridadService.listarCargoEstudiantil({idTipoCargoEstudiantil: this.unidadAutoridad.nivel, tipoEstamento: 1})
          .subscribe(
            (data2: any ) => {
              this.cargoEstudiantil = data2;
            });
        });
    this.autoridadService.listarTipoCargoNivel()
    .subscribe(
      (data: any ) => {
        this.tipoCargoNivel = data;
      });
    this.autoridadService.listarTipoCargoNro()
    .subscribe(
      (data: any ) => {
        this.tipoCargoNro = data;
      });
  }
  volver() {
    this.router.navigate(['autoridad', this.idUnidad]);
  }
  buscarEstudiantes() {
    console.log(this.busquedaCi);
    this.autoridadService.listarEstudiantesMatriculacion(this.busquedaCi, this.authenticationService.getUser())
      .subscribe(
        (data: any) => {
          console.log(data);
          this.estudiantes = data;
        }
      );
  }
  cambiarFormulario(idEstudianteCargo: number, idEstudiante: number) {
    this.obtenerEstudiante(idEstudiante);
    this.formularioCompleto = false;
  }
  obtenerEstudiante(idEstudiante: number) {
    this.estudianteService.obtenerEstudiante(idEstudiante)
      .subscribe(
        (data: any) => {
          this.estudiante = data;
          console.log(data);
        }
      );
  }
  obtenerUnidadAutoridad() {
    this.estudianteService.obtenerUnidadAcademica(this.idUnidad)
      .subscribe(
        (data: any) => console.log(data)
      );
  }
  registrarEstudianteCargo(idEstudianteCargo: number) {
    this.estudianteCargo.idUnidadAutoridad = this.idUnidad;
    if ( this.activatedRoute.snapshot.params.idEstudianteCargo === '0') {
      this.estudianteCargo.idEstudiante = this.estudiante.idEstudiante;
    }
    // console.log(JSON.stringify(this.estudianteCargo));
    this.autoridadService.registrarEstudianteCargo(this.estudianteCargo)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            this.router.navigate(['autoridad', this.idUnidad]);
          }
        }
      )
      ;
  }
}
