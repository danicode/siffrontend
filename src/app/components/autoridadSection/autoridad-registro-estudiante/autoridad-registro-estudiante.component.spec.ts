import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoridadRegistroEstudianteComponent } from './autoridad-registro-estudiante.component';

describe('AutoridadRegistroEstudianteComponent', () => {
  let component: AutoridadRegistroEstudianteComponent;
  let fixture: ComponentFixture<AutoridadRegistroEstudianteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoridadRegistroEstudianteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoridadRegistroEstudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
