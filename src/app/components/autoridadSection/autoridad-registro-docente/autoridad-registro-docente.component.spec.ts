import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoridadRegistroDocenteComponent } from './autoridad-registro-docente.component';

describe('AutoridadRegistroDocenteComponent', () => {
  let component: AutoridadRegistroDocenteComponent;
  let fixture: ComponentFixture<AutoridadRegistroDocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoridadRegistroDocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoridadRegistroDocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
