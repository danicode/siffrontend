import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { AutoridadService } from '../../../services/data/autoridad.service';
import { DocenteService } from '../../../services/data/docente.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/public_api';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-autoridad-registro-docente',
  templateUrl: './autoridad-registro-docente.component.html',
  styleUrls: ['./autoridad-registro-docente.component.css']
})
export class AutoridadRegistroDocenteComponent implements OnInit {

  //
  docenteCargo: any = {};
  busquedaCi: string;
  docentes: any[];
  docente: any = {};
  idUnidad: number;
  unidadAutoridad: any = {};
  cargoEstudiantil: any [];
  tipoCargoNivel: any [];
  tipoCargoNro: any [];
  
  // para fechas
  datePickerConfig: Partial<BsDatepickerConfig>;
  formularioCompleto = true;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private autoridadService: AutoridadService,
    private docenteService: DocenteService,
    private bsLocaleService: BsLocaleService

  ) {
    // para fechas
    this.bsLocaleService.use('es');
   }

  ngOnInit(): void {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: false,
        dateInputFormat: 'DD/MM/YYYY'
      });
    this.idUnidad = this.activatedRoute.snapshot.params.idUnidadAutoridad;
    this.docenteCargo.idDocenteCargo = this.activatedRoute.snapshot.params.idDocenteCargo;
    if (this.docenteCargo.idDocenteCargo !== '0') {
      this.formularioCompleto = false;
    }
    this.autoridadService.obtenerDocenteCargo(this.docenteCargo.idDocenteCargo)
      .subscribe(
        (data: any) => {
          this.docenteCargo = data;
          if ( this.docenteCargo.idDocenteCargo !== 0) {
            this.docente.nombreCompleto = this.docenteCargo.nombreCompleto;
            this.docente.unidadAcademica = this.docenteCargo.unidadAcademica;
            this.docente.ru = this.docenteCargo.ru;
          }
        });

    this.docenteService.obtenerUnidadAcademica(this.idUnidad)
      .subscribe(
        (data: any ) => {
          this.unidadAutoridad = data;
          this.autoridadService.listarCargoEstudiantil({idTipoCargoEstudiantil: this.unidadAutoridad.nivel, tipoEstamento:2})
          .subscribe(
            (data2: any ) => {
              this.cargoEstudiantil = data2;
            });
        });
    this.autoridadService.listarTipoCargoNivel()
    .subscribe(
      (data: any ) => {
        this.tipoCargoNivel = data;
      });
    this.autoridadService.listarTipoCargoNro()
    .subscribe(
      (data: any ) => {
        this.tipoCargoNro = data;
      });
  }
  volver() {
    this.router.navigate(['autoridad', this.idUnidad]);
  }
  buscarDocentes() {
    this.autoridadService.listarDocentesWebService(this.busquedaCi, this.authenticationService.getUser())
      .subscribe(
        (data: any) => {
          console.log(data);
          this.docentes = data;
        }
      );
  }
  cambiarFormulario(idDocenteCargo: number, idDocente: number) {
    this.obtenerDocente(idDocente);
    this.formularioCompleto = false;
  }
  obtenerDocente(idDocente: number) {
    this.docenteService.obtenerDocente(idDocente)
      .subscribe(
        (data: any) => {
          this.docente = data;
          console.log(data);
        }
      );
  }
  obtenerUnidadAutoridad() {
    this.docenteService.obtenerUnidadAcademica(this.idUnidad)
      .subscribe(
        (data: any) => console.log(data)
      );
  }
  registrarDocenteCargo(idDocenteCargo: number) {
    this.docenteCargo.idUnidadAutoridad = this.idUnidad;
    if ( this.activatedRoute.snapshot.params.idDocenteCargo === '0') {
      this.docenteCargo.idDocente = this.docente.idDocente;
    }
    // console.log(JSON.stringify(this.docenteCargo));
    this.autoridadService.registrarDocenteCargo(this.docenteCargo)
      .subscribe(
        (data: any) => {
          if (data.code === 1) {
            this.router.navigate(['autoridad', this.idUnidad]);
          }
        }
      )
      ;
  }

}
