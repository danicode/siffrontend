import { Component, OnInit } from '@angular/core';
import { EstudianteService } from '../../../services/data/estudiante.service';
import { PersonaService } from '../../../services/data/persona.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { AutoridadService } from '../../../services/data/autoridad.service';

@Component({
  selector: 'app-estudiante-registro',
  templateUrl: './estudiante-registro.component.html',
  styleUrls: ['./estudiante-registro.component.css']
})
export class EstudianteRegistroComponent implements OnInit {
  estudiante: any = {};
  unidades: any[];
  error = false;
  errorMensaje: string;
  constructor(
    private estudianteService: EstudianteService,
    private autoridadService: AutoridadService,
    private personaService: PersonaService,
    private route: Router,
    private activeRoute: ActivatedRoute,
    private authenticationService: AuthenticationService
  ) { }
  ngOnInit(): void {
    this.estudianteService.listarUnidadesAcademicas({nivel: 3})
      .subscribe(
        (data: any) => {
          this.unidades = data;
        }

      ),

    this.estudiante = {
        idEstudiante: this.activeRoute.snapshot.params.idEstudiante,
        idPersona: this.activeRoute.snapshot.params.idPersona
    };
    if (this.estudiante.idEstudiante === '0') {
      this.personaService.obtenerPersona(this.estudiante.idPersona)
      .subscribe(
        (data: any) => {
          this.estudiante = {
            nombreCompleto: data.nombreCompleto,
            idPersona: this.activeRoute.snapshot.params.idPersona,
            idUnidad: null
          };
        });
    } else {
      this.estudianteService.obtenerEstudiante(this.estudiante.idEstudiante)
      .subscribe(
        (data: any) => {
          this.estudiante = data;
        });
    }
  }
  volver() {
    this.route.navigate(['estudiante']);
  }
  registrarEstudiante(idEstudiante: number) {
    this.estudiante.idEstudiante = idEstudiante === 0 ? null : idEstudiante;
    this.estudiante.usuReg = this.authenticationService.getUser();
    this.estudiante.usuMod = this.authenticationService.getUser();
    this.estudianteService.registrarEstudiante(this.estudiante)
      .subscribe(
        (data: any ) => {
          if (data.code === 1) {
            this.volver();
          } else {
            this.error = true;
            this.errorMensaje = data.message;
          }
        });
  }
}
