import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../../services/data/persona.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'app-estudiante-persona',
  templateUrl: './estudiante-persona.component.html',
  styleUrls: ['./estudiante-persona.component.css']
})
export class EstudiantePersonaComponent implements OnInit {

  // Variables de Administracion
  personas: any[];
  persona: any;
  idPersona: number;
  // Variables de Interfaz
  searchText: string;
  p = 1;
  error = false;
  errorMensaje: string;
  constructor(
    private personaService: PersonaService,
    private route: Router
  ) { }
  ngOnInit(): void {
    this.persona = {
      // idPersona: null
    };
    this.personaService.listarPersonas(this.persona)
      .subscribe(
        (data: any) => this.personas = data
      );
  }
  volver() {
    this.route.navigate(['estudiante']);
  }
  seleccionarPersona(idPersona: number) {
    this.route.navigate(['estudiante', 0, idPersona]);
  }

}
