import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstudiantePersonaComponent } from './estudiante-persona.component';

describe('EstudiantePersonaComponent', () => {
  let component: EstudiantePersonaComponent;
  let fixture: ComponentFixture<EstudiantePersonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstudiantePersonaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstudiantePersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
